const buttonToggle = document.querySelector("header nav img.btn-toggle")
const toggle = document.querySelector("header .toggle")
const body = document.querySelector("body")

buttonToggle.addEventListener("click", () => {
    if (toggle.style.display == 'block') {
        toggle.style.display = 'none'
        buttonToggle.src = '/assets/icons/btn-toggle.png'
    } else {
        toggle.style.display = 'block'
        buttonToggle.src = '/assets/icons/btn-close.png'
    }
})