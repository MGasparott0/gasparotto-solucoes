const express = require("express")
const server = express()

//configurar pasta publica
server.use(express.static("public"))

//Habilirar o uso do req.body na nossa aplicação
server.use(express.urlencoded({ extended: true }))

//Utilizando template engine
const nunjucks = require("nunjucks")
nunjucks.configure("src/views", {
    express: server,
    noCache: true
})

//Configurar caminhos da minha aplicação
//página inicial
//req: requisição
//res: resposta
server.get("/", (req, res) => {
    return res.render("index.html", {
        title: "Gasparotto Soluções"
    })
})

server.get("/contact", (req, res) => {
    return res.render("contact.html", {
        title: "Gasparotto Soluções"
    })
})

server.get("/about", (req, res) => {
    return res.render("about.html", {
        title: "Gasparotto Soluções"
    })
})

//ligar o servidor
server.listen(3000)